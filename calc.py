#!/usr/bin/python3

import sys
import decimal
import operator

# Map string to operation
operations_map = {
    "suma": operator.add,
    "resta": operator.sub,
    "multiplicacion": operator.mul,
    "division": operator.truediv,
}

# Check entry
if(len(sys.argv) != 4):
    sys.exit("Usage error: $ calc.py Operation Arg1 Arg2")

operation = sys.argv[1]
if operation not in operations_map:
    sys.exit("Operation not supported")

try:
    op1 = decimal.Decimal(sys.argv[2])
    op2 = decimal.Decimal(sys.argv[3])
except decimal.InvalidOperation as e:
    sys.exit("Error: ARGs must be inbounds python decimal number\n{}".format(e))

# Main
try:
    result =  operations_map[operation](op1,op2)
except decimal.Overflow:
    sys.exit("Error: Operation overflow")
except decimal.DivisionByZero:
    result = "Error: Division by Zero not allowed"

print("result = {}".format(result))
exit(0)
